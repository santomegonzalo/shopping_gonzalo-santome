# Farfetch Challenge

### What I used

I created this small app using `create-react-app` to set the focus on the functionality and not on how to config webpack. The app is using the latest version or `React` with `Redux` and `Redux Thunk`.
There is no API to get the data, so I decided to create a small `mock` with a timeout inside `api.js` to simulate a real REST request from a json file, on production this need to be improved to use `axios` or another package to get the data from some external API.

### Idea behind

The idea of the challenge is to create a small **bag** with only 4 products to simulate a real bag, every time the user adds a new item, I will increase or decrease the total amount to pay.

There were some typical questions for a PM but I decided to continue, **but in a real case I will always ask to a Product Manager**.
- The list of products need to be in a new page? The challenge specify one page but at the begining I decided to create two pages using my own implementation of `react-router` because I don't like how `react-router` works, on this [commit](https://bitbucket.org/santomegonzalo/shopping_gonzalo-santome/commits/b6a9ee41bf2a0c590f191450b0b0e60a45e64c5d) you could find my implementation using `page.js`, but then I rollback to just have on page.
- What happened if the user wanted to add two times the same product? The challenge didn't specified this, so I decided to only support one item per product on your Bag.
- Do we want to keep the scroll position on adding a new product to the Bag? This could be possible just with a simple javascript function but I didn't dig deep into it because I decided to put my focus on the logic.

### Things to consider

- The Challenge says "files less than 100kb", that's why I decided to move `react-dom` into a chunk file. In a real production case, I will move more packages into a different chunk.
- I could tried to don't use redux and use the new `Context` api from `React` but I'm still testing it on my private repos.
- It will be nice to include `Flow` but I think is not part of the scope.

### How to run it
I'm using node@8.11.3 on my machines to support the latest `eslint`.

- `yarn install`
- `yarn test`: run all the tests
- `yarn start`: start webpack server to serve the files on debug mode using `.map`
- `yarn build`: generates all the build files into `build` folder
- `yarn analyze`: will lunch a new website to analyze the packages into my build files

### Folder architecture

- `build`: includes final files including `html` `minified js` and `fonts`
- `src`: includes all of our javascript logic
  - `assets`: folder to save our icons, images or config scss files
    - `scss`: folder where I have my scss config files like colors, font names, and variables
  - `components`: folder to include generic components that we could reuse everywhere inside our app. Every component follows this structure: `__tests__` folder, `index.js` where the component lives and `styles.modules.scss` CSS Module for the component
  - `locales`: folder to include all different languages
  - `mock`: folder where I created my own mocks
  - `modules`: folder to include every section that needs a connection to redux, using the following structure: `components`, `pages` and `reducers`
  - `redux`: folder to include the initialization of redux
  - `utils`: folder to include all needed helpers
