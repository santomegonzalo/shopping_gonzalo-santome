import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import Header from '../index';

describe('Header', () => {
  const mapTestId = {
    header: '[data-testid="Header"]',
  };

  it('compare with snapshots', () => {
    expect(shallow(<Header />)).toMatchSnapshot();
    expect(shallow(<Header title="Header Test" />)).toMatchSnapshot();
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Header />, div);
  });

  it('renders with title', () => {
    const title = 'React Title';

    const component = shallow(<Header title={title} />);

    expect(component.find(mapTestId.header)).toExist();
    expect(component.find(mapTestId.header)).toHaveText(title);
  });
});
