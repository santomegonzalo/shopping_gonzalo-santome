import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.css';

const Header = ({ title }) => (
  <header className={styles.Header} data-testid="Header">
    {title}
  </header>
);

Header.propTypes = {
  title: PropTypes.string,
};

export default Header;
