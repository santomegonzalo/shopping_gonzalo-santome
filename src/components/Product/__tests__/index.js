import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import { getNumberWithCurrency } from 'utils/currency';
import { calculateTotalPrice } from 'utils/product';

import Product from '../index';

describe('Product', () => {
  const product = {
    name: 'Striped peplum top',
    brand: 'TANYA TAYLOR',
    price: 1585,
    discount: 50,
    currency: '£',
  };

  const mapTestId = {
    product: '[data-testid="Product"]',
    productContent: '[data-testid="ProductContent"]',
    productAction: '[data-testid="ProductAction"]',
    productPriceDiscounted: '[data-testid="ProductPriceDiscounted"]',
  };

  it('compare with snapshots', () => {
    expect(shallow(<Product />)).toMatchSnapshot();
    expect(shallow(<Product product={product} />)).toMatchSnapshot();
    expect(shallow(<Product product={product} actionName="Action" actionClick={() => { }} />)).toMatchSnapshot();
  });

  it('full renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Product />, div);
  });

  it('component renders without crashing', () => {
    const component = shallow(<Product />);

    expect(component.find(mapTestId.product)).not.toExist();
  });

  it('render product without action', () => {
    const component = shallow(<Product product={product} />);

    expect(component.find(mapTestId.productContent)).toExist();
    expect(component.findWhere(n => n.text() === product.name).first()).toHaveText(product.name);
    expect(component.findWhere(n => n.text() === product.brand).first()).toHaveText(product.brand);
    expect(component.find(mapTestId.productPriceDiscounted)).toHaveText(getNumberWithCurrency(calculateTotalPrice(product), product.currency));
    expect(component.find(mapTestId.productAction)).not.toExist();
  });

  it('render product with action', () => {
    const component = shallow(<Product product={product} actionName="Action" actionClick={() => {}} />);

    expect(component.find(mapTestId.productContent)).toExist();
    expect(component.find(mapTestId.productAction)).toExist();
  });

  it('simulate action event', () => {
    const onActionClick = sinon.spy();
    const component = shallow(<Product product={product} actionName="Action" actionClick={onActionClick} />);
    component.find('RoundedButton').simulate('click');
    expect(onActionClick.calledOnce).toEqual(true);
    expect(onActionClick.calledTwice).toEqual(false);
  });
});
