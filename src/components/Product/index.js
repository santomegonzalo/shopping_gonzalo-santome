import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { calculateTotalPrice } from 'utils/product';
import { getNumberWithCurrency } from 'utils/currency';

import RoundedButton from '../RoundedButton';

import styles from './styles.module.css';

class Product extends PureComponent {
  handleActionClick = () => {
    const { actionClick, product } = this.props;

    actionClick(product);
  }

  renderPrice() {
    const { product } = this.props;

    return (
      <span className={styles.Product__PriceRegular}>
        {getNumberWithCurrency(product.price, product.currency)}
      </span>
    );
  }

  renderPriceDiscount() {
    const { product } = this.props;

    return (
      <React.Fragment>
        <div className={styles.Product__PriceOriginal}>
          {getNumberWithCurrency(product.price, product.currency)}
        </div>
        <div className={styles.Product__PriceDiscounted} data-testid="ProductPriceDiscounted">
          {getNumberWithCurrency(calculateTotalPrice(product), product.currency)}
        </div>
        <div className={styles.Product__PricePercentage}>
          {`${product.discount}% off`}
        </div>
      </React.Fragment>
    );
  }

  render() {
    const {
      product,
      actionName,
      actionClick,
    } = this.props;

    if (!product) {
      return null;
    }

    return (
      <article className={styles.Product} data-testid="Product">
        <div className={styles.Product__Content} data-testid="ProductContent">
          <img className={styles.Product__Image} src={product.image} alt={product.name} />
          <h2 className={styles.Product__Brand}>
            {product.brand}
          </h2>
          <h1 className={styles.Product__Name}>
            {product.name}
          </h1>
          <div className={styles.Product__Price}>
            {product.discount ? this.renderPriceDiscount() : this.renderPrice()}
          </div>
        </div>
        {
          actionName && actionClick && (
            <div className={styles.Product__Action} data-testid="ProductAction">
              <RoundedButton text={actionName} onClick={this.handleActionClick} />
            </div>
          )
        }
      </article>
    );
  }
}

Product.propTypes = {
  actionName: PropTypes.string,
  actionClick: PropTypes.func,
  product: PropTypes.object,
};

export default Product;
