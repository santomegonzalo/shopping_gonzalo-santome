import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import Products from '../index';

describe('Products', () => {
  const products = [{
    id: 1,
    name: 'bicolour pleated skirt',
    brand: 'VIONNET',
    price: 682.76,
    currency: '£',
  }, {
    id: 2,
    name: 'varsity bomber jacket',
    brand: 'FORTE COUTURE',
    price: 480.90,
    currency: '£',
  }];

  const mapTestId = {
    productsLoading: '[data-testid="ProductsLoading"]',
    productsMessage: '[data-testid="ProductsMessage"]',
  };

  it('compare with snapshots', () => {
    expect(shallow(<Products isLoading emptyText="Foo" />)).toMatchSnapshot();
    expect(shallow(<Products isLoading={false} />)).toMatchSnapshot();
    expect(shallow(<Products isLoading={false} emptyMessage="An empty message" />)).toMatchSnapshot();
    expect(shallow(<Products products={products} />)).toMatchSnapshot();
  });

  it('full renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Products />, div);
  });

  it('render loading indicator without emptyText', () => {
    const component = shallow(<Products isLoading emptyText="Foo" />);

    expect(component.find(mapTestId.productsLoading)).toExist();
    expect(component.find(mapTestId.productsMessage)).not.toExist();
  });

  it('hide loading indicator', () => {
    const component = shallow(<Products isLoading={false} />);

    expect(component.find(mapTestId.productsLoading)).not.toExist();
  });

  it('render empty message', () => {
    const text = 'Nothing to show';
    const component = shallow(<Products isLoading={false} emptyMessage={text} />);

    expect(component.find(mapTestId.productsMessage)).toExist();
    expect(component.find(mapTestId.productsMessage)).toHaveText(text);
  });

  it('render products', () => {
    const component = shallow(<Products products={products} />);

    expect(component.find('Product').length).toEqual(products.length);
  });
});
