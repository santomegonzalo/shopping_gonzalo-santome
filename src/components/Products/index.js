import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { isNil } from 'utils/object';
import { t } from 'utils/language';

import Product from '../Product';

import styles from './styles.module.css';

class Products extends PureComponent {
  render() {
    const {
      style,
      products,
      isLoading,
      emptyMessage,
      actionName,
      actionClick,
    } = this.props;

    return (
      <section className={styles.Products} style={style}>
        {
          isLoading
          && (
            <div className={styles.Products__Message} data-testid="ProductsLoading">
              {t('products.loading_text')}
            </div>
          )
        }
        {
          !isLoading && products.length === 0 && !isNil(emptyMessage)
          && (
            <div className={styles.Products__Message} data-testid="ProductsMessage">
              {emptyMessage}
            </div>
          )
        }
        {
          products.map(product => (
            <Product
              key={product.id}
              product={product}
              actionName={actionName}
              actionClick={actionClick}
            />
          ))
        }
      </section>
    );
  }
}

Products.defaultProps = {
  style: {},
  products: [],
  isLoading: false,
};

Products.propTypes = {
  isLoading: PropTypes.bool,
  products: PropTypes.array,
  emptyMessage: PropTypes.string,
  actionName: PropTypes.string,
  actionClick: PropTypes.func,
  style: PropTypes.object,
};

export default Products;
