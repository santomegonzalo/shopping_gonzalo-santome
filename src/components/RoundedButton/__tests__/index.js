import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import RoundedButton from '../index';

describe('RoundedButton', () => {
  it('compare with snapshots', () => {
    expect(shallow(<RoundedButton />)).toMatchSnapshot();
    expect(shallow(<RoundedButton text="A simple button" />)).toMatchSnapshot();
    expect(shallow(<RoundedButton text="A simple button" onClick={() => {}} />)).toMatchSnapshot();
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<RoundedButton />, div);
  });

  it('renders with text', () => {
    const text = 'React Button';

    const component = shallow(<RoundedButton text={text} onClick={() => {}} />);

    expect(component).toExist();
    expect(component).toHaveText(text);
  });

  it('simulate action event', () => {
    const text = 'React Button';
    const onClick = sinon.spy();
    const component = shallow(<RoundedButton text={text} onClick={onClick} />);
    component.simulate('click');

    expect(onClick.calledOnce).toEqual(true);
    expect(onClick.calledTwice).toEqual(false);
  });
});
