import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.css';

const RoundedButton = ({ text, onClick }) => {
  if (!text || !onClick) {
    return null;
  }

  return (
    <button type="button" className={styles.RoundedButton} onClick={onClick}>
      {text}
    </button>
  );
};

RoundedButton.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

export default RoundedButton;
