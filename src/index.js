import React from 'react';
import { Provider } from 'react-redux';

import './index.css';

// We dont need to use any routes for now
import BagPage from './modules/bag/pages/BagPage';

import store from './redux/store';
import registerServiceWorker from './registerServiceWorker';

// ReactDOM is too big to have it on the main file, so I decided to create a chunk only with that file
import('react-dom')
  .then((ReactDOM) => {
    ReactDOM.render(
      <Provider store={store}>
        <BagPage />
      </Provider>,
      document.getElementById('root'),
    );
  });

registerServiceWorker();
