import ProductsJSON from './products.json';

const guid = () => {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
};

export const listProducts = () => {
  const imagesName = [
    '11730556_8194858_255.jpg',
    '11734827_8179707_255.jpg',
    '11735583_8161812_255.jpg',
    '11736243_8231608_255.jpg',
    '11745743_8214817_255.jpg',
  ];

  const { products } = ProductsJSON;

  return products.map((product, index) => {
    const id = guid();

    return {
      ...product,
      image: require(`./images/${imagesName[index]}`), // eslint-disable-line
      id,
    };
  });
};
