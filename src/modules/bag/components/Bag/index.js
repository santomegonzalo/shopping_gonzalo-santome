import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { t } from 'utils/language';

import Header from 'components/Header';
import Products from 'components/Products';

import { removeFromBag } from '../../reducers/actions';

class Bag extends PureComponent {
  handleRemoveFromBag = (product) => {
    const { dispatchRemoveFromBag } = this.props;

    dispatchRemoveFromBag(product);
  };

  render() {
    const { products } = this.props;

    return (
      <React.Fragment>
        <Header title={t('bag.title_label')} />
        <Products
          style={{
            marginBottom: '30px',
          }}
          emptyMessage={t('bag.empty_text')}
          products={products}
          actionName={t('bag.remove_action')}
          actionClick={this.handleRemoveFromBag}
        />
      </React.Fragment>
    );
  }
}

Bag.propTypes = {
  products: PropTypes.array,
  dispatchRemoveFromBag: PropTypes.func,
};

const mapStateToProps = ({ bag }) => ({
  products: bag.products,
});

const mapDispatchToProps = dispatch => ({
  dispatchRemoveFromBag: product => dispatch(removeFromBag(product)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Bag);
