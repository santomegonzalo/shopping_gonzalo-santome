import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme'; // eslint-disable-line import/no-extraneous-dependencies

import { getNumberWithCurrency } from 'utils/currency';

import { BagSummary } from '../index';

describe.only('BagSummary', () => {
  const products = [{
    id: 1,
    name: 'bicolour pleated skirt',
    brand: 'VIONNET',
    price: 682.76,
    currency: '£',
  }, {
    id: 2,
    name: 'varsity bomber jacket',
    brand: 'FORTE COUTURE',
    price: 480.90,
    currency: '£',
  }];

  const mapTestId = {
    bagSummaryTotal: '[data-testid="BagSummaryTotal"]',
  };

  it('compare with snapshots', () => {
    expect(shallow(<BagSummary />)).toMatchSnapshot();
    expect(shallow(<BagSummary products={products} />)).toMatchSnapshot();
  });

  it('full renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<BagSummary />, div);
  });

  it('render price total', () => {
    const component = shallow(<BagSummary products={products} />);
    const totalPrice = products.reduce((accum, p) => accum + p.price, 0);

    expect(component.find(mapTestId.bagSummaryTotal)).toIncludeText(getNumberWithCurrency(totalPrice, '£'));
  });
});
