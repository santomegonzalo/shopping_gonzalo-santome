import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { currencyCodeBySymbolMap, getNumberWithCurrency } from 'utils/currency';
import { calculateTotalPrice } from 'utils/product';
import { t } from 'utils/language';

import styles from './styles.module.css';

export class BagSummary extends PureComponent {
  getTotal() {
    const { products } = this.props;

    if (products.length === 0) {
      return 0;
    }

    return getNumberWithCurrency(products.reduce((accum, product) => accum + calculateTotalPrice(product), 0), products[0].currency);
  }

  render() {
    const { products } = this.props;
    const currencySymb = products.length > 0 ? products[0].currency : '';
    const currencyLabel = currencyCodeBySymbolMap[currencySymb];

    return (
      <section className={styles.BagSummary}>
        <div className={styles.BagSummary__Top}>
          <div className={styles.BagSummary__Column}>
            <span className={styles.BagSummary__Title}>
              {t('bag_summary.title_label')}
            </span>
          </div>
          <div className={[styles.BagSummary__Column, styles.BagSummary__Column_Right]}>
            <span className={styles.BagSummary__Small}>
              {`${t('bag_summary.quantity_label')} ${products.length}`}
            </span>
          </div>
        </div>
        <div className={styles.BagSummary__Bottom}>
          <div className={styles.BagSummary__Column}>
            <span className={`${styles.BagSummary__Text} ${styles.BagSummary__TextBold}`}>
              {t('bag_summary.total_label')}
            </span>
          </div>
          <div className={`${styles.BagSummary__Column} ${styles.BagSummary__Column_Right}`}>
            <div className={styles.BagSummary__Text}>
              <span>
                {currencyLabel}
                &nbsp;
              </span>
              <span className={`${styles.BagSummary__Text} ${styles.BagSummary__TextBold}`} data-testid="BagSummaryTotal">
                {this.getTotal()}
              </span>
            </div>
            <span className={styles.BagSummary__Small}>
              {t('bag_summary.tax_label')}
            </span>
          </div>
        </div>
      </section>
    );
  }
}

BagSummary.defaultProps = {
  products: [],
};

BagSummary.propTypes = {
  products: PropTypes.array,
};

const mapStateToProps = ({ bag }) => ({
  products: bag.products,
});

export default connect(mapStateToProps)(BagSummary);
