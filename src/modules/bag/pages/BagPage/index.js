import React from 'react';

import Bag from '../../components/Bag';
import BagSummary from '../../components/BagSummary';
import ProductList from '../../../product-list/components/ProductList';

import styles from './styles.module.css';

const BagPage = () => (
  <section className={styles.BagPage}>
    <section className={styles.BagPage__Content}>
      <Bag />
      <ProductList />
    </section>
    <aside className={styles.BagPage__Sidebar}>
      <BagSummary />
    </aside>
  </section>
);

export default BagPage;
