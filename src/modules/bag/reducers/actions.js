import { BAG_ADD, BAG_REMOVE } from './actionTypes';

export const addToBag = product => ({
  type: BAG_ADD,
  data: {
    product,
  },
});

export const removeFromBag = product => ({
  type: BAG_REMOVE,
  data: {
    product,
  },
});
