import { BAG_ADD, BAG_REMOVE } from './actionTypes';

const initialState = {
  products: [],
};

export default function bag(state = initialState, action = {}) {
  switch (action.type) {
    case BAG_ADD: {
      const { products } = state;
      const productIndex = products.findIndex(p => p.id === action.data.product.id);

      if (productIndex >= 0) {
        return state;
      }

      return {
        products: [
          ...state.products,
          action.data.product,
        ],
      };
    }
    case BAG_REMOVE: {
      const { products } = state;
      const productIndex = products.findIndex(p => p.id === action.data.product.id);

      return {
        products: [
          ...products.slice(0, productIndex),
          ...products.slice(productIndex + 1),
        ],
      };
    }
    default: {
      return state;
    }
  }
}
