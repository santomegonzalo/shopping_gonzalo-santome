import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { t } from 'utils/language';

import Header from 'components/Header';
import Products from 'components/Products';

import { addToBag } from '../../../bag/reducers/actions';
import { loadProducts } from '../../reducers/actions';

class ProductList extends PureComponent {
  componentDidMount() {
    const { dispatchLoadProducts } = this.props;

    dispatchLoadProducts();
  }

  handleAddToBag = (product) => {
    const { dispatchAddToBag } = this.props;

    dispatchAddToBag(product);
  };

  render() {
    const { isLoading, products } = this.props;

    return (
      <React.Fragment>
        <Header title={t('product_list.title_label')} />
        <Products
          products={products}
          isLoading={isLoading}
          actionName={t('product_list.add_action')}
          actionClick={this.handleAddToBag}
        />
      </React.Fragment>
    );
  }
}

ProductList.propTypes = {
  products: PropTypes.array,
  isLoading: PropTypes.bool,
  dispatchLoadProducts: PropTypes.func,
  dispatchAddToBag: PropTypes.func,
};

const mapStateToProps = ({ productList }) => ({
  products: productList.products,
  isLoading: productList.isLoading,
});

const mapDispatchToProps = dispatch => ({
  dispatchLoadProducts: () => dispatch(loadProducts()),
  dispatchAddToBag: product => dispatch(addToBag(product)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
