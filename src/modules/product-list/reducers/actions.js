import { get } from 'utils/api';

import { PRODUCT_LOADING_STATUS, PRODUCT_LOADED } from './actionTypes';

const PRODUCTS_URL = ''; // @todo replace with real api

const setLoadingStatus = loading => ({
  type: PRODUCT_LOADING_STATUS,
  data: {
    loading,
  },
});

const productsLoaded = products => ({
  type: PRODUCT_LOADED,
  data: {
    products,
  },
});

export const loadProducts = () => (
  async (dispatch) => {
    dispatch(setLoadingStatus(true));

    const response = await get(PRODUCTS_URL);

    dispatch(productsLoaded(response.products));
  }
);
