import { PRODUCT_LOADED, PRODUCT_LOADING_STATUS } from './actionTypes';

const initialState = {
  products: [],
  isLoading: true,
};

export default function productList(state = initialState, action = {}) {
  switch (action.type) {
    case PRODUCT_LOADED: {
      return {
        products: [
          ...state.products,
          ...action.data.products,
        ],
        isLoading: false,
      };
    }
    case PRODUCT_LOADING_STATUS: {
      return {
        ...state,
        isLoading: action.data.loading,
      };
    }
    default: {
      return state;
    }
  }
}
