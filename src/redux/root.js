import { combineReducers } from 'redux';

import productList from '../modules/product-list/reducers';
import bag from '../modules/bag/reducers';

export default combineReducers({
  productList,
  bag,
});
