/* eslint-disable import/no-extraneous-dependencies */
// import 'react-testing-library/cleanup-after-each';
// import 'jest-dom/extend-expect';

import 'jest-enzyme';

import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
