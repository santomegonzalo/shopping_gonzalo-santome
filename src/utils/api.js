export const get = (url) => { // eslint-disable-line no-unused-vars
  if (process.env.NODE_ENV !== 'production') {
    return import('mock/products')
      .then(({ listProducts }) => {
        return new Promise(resolve => setTimeout(() => resolve({ products: listProducts() }), 1000));
      });
  }

  // @todo Implement a real fetch api.
  return Promise.resolve({ products: [] });
};
