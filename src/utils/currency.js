import { isNil } from './object';

export const currencyCodeBySymbolMap = {
  '£': 'GBP',
  '$': 'DOL', // eslint-disable-line quote-props
  '€': 'EUR',
  '': '',
};

export const getNumberWithCurrency = (number, symbol) => {
  if (isNil(number)) {
    return 0;
  }

  return new Intl.NumberFormat(navigator.language, { style: 'currency', currency: currencyCodeBySymbolMap[symbol] }).format(number);
};
