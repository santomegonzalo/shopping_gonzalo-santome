import get from 'lodash.get';

import en from '../locales/en/messages.json'; // eslint-disable-line

export const t = key => get(en, key);
