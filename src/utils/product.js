export const calculateTotalPrice = (product) => {
  let productPrice = product.price;

  if (product.discount) {
    productPrice = (product.price - ((product.price * product.discount) / 100));
  }

  return productPrice;
};
